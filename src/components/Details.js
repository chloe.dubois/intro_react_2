import React, { useState, useEffect } from 'react';
import {unstable_batchedUpdates} from 'react-dom';

const Details = ({ match }) => {
  const {
    params: { movieId },
  } = match;

  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [movie, setMovie] = useState([]);

  useEffect(() => {
    const apikey = '99f14a8ad1455167c344aa55890de367';
    const urlMovie = `https://api.themoviedb.org/3/movie/${movieId}?api_key=${apikey}&append_to_response=credits,images,keywords,recommendations`;
    fetch(urlMovie)
      .then(res => res.json())
      .then(
        (result) => {
          unstable_batchedUpdates (()=> {
            setIsLoaded(true);
            setMovie(result);
          })
          console.log(result)
        },
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      )
  }, [movieId])

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    const year = new Date(movie.release_date).getFullYear();
    const monthInLetters = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    const month = monthInLetters[new Date(movie.release_date).getMonth()];
    const day = new Date(movie.release_date).getDate();
    const genres = []
    movie.genres.forEach((entry) => {
        genres.push(entry.name);
    });
    return (
        <div>
            <img className="details-poster" src={`https://image.tmdb.org/t/p/w154${movie.poster_path}`} alt="backdrop" />
            <h1>{movie.title} ({year})</h1>
            <strong><i>{movie.tagline}</i></strong>
            <p>{movie.vote_average}/{movie.vote_count} votes</p>
            <p><i>{movie.overview}</i></p>
            <p><strong>Date of release:</strong> {day} {month} {year}</p>
            <p><strong>Duration:</strong> {movie.runtime} min</p>
            <p><strong>Genres:</strong> {genres.join(', ')}</p>
        </div>
      );
  }
}

export default Details;