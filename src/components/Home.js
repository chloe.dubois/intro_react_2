import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";

export default function Home() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [movies, setMovies] = useState([]);
    const apikey = '99f14a8ad1455167c344aa55890de367';
    const selection = 'now_playing';

    useEffect(() => {
        fetch(`https://api.themoviedb.org/3/movie/${selection}?api_key=${apikey}`)
          .then(res => res.json())
          .then(
            (result) => {
              setIsLoaded(true);
              setMovies(result.results);
            },
            (error) => {
              setIsLoaded(true);
              setError(error);
            }
          )
      }, [])
    
      if (error) {
        return <div>Error: {error.message}</div>;
      } else if (!isLoaded) {
        return <div>Loading...</div>;
      } else {
        return (
            <div className="movies-container">
              {movies.map(item => (
                <div className="movies-poster" key={item.id}>
                    <Link to={`/details/${item.id}`}>
                        <img src={`https://image.tmdb.org/t/p/w154${item.poster_path}`} alt="poster" />
                    </Link>
                </div>
              ))}
            </div>
          );
      }
    }
  