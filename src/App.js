import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Home from './components/Home';
import Upcoming from './components/Upcoming';
import TopRated from './components/TopRated';
import Details from './components/Details';

export default function App() {
  return (
    <Router>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Now playing</Link>
            </li>
            <li>
              <Link to="/upcoming">Upcoming</Link>
            </li>
            <li>
              <Link to="/top-rated">Top rated</Link>
            </li>
          </ul>
        </nav>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/upcoming" exact component={Upcoming} />
          <Route path="/top-rated" exact component={TopRated} />
          <Route path="/details/:movieId" exact component={Details} />
        </Switch>
      </div>
    </Router>
  );
}